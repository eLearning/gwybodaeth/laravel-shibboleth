<?php

namespace Tests\Feature;

use App\User;
use Orchestra\Testbench\TestCase;
use StudentSystemServices\Shibboleth\Controllers\ShibbolethController;
use StudentSystemServices\Shibboleth\Tests\Stubs\Setup;

class ShibbolethControllerTest extends TestCase
{
    use Setup;

    public function test_creates_user()
    {
        $getJeff = function(){
            return User::where('email', 'jeff@example.org')->first();
        };

        $this->assertEmpty($getJeff());

        User::create([
            'name' => 'jeff',
            'email' => 'jeff@example.org',
            'password' => 'password',
        ]);

        $this->assertInstanceOf(User::class, $getJeff());

        (new ShibbolethController)->idpAuthenticate();

        $this->assertSame('100000001', $getJeff()->netid);
    }
}
