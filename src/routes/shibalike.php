<?php
Route::group(['middleware' => 'web'], function () {
    Route::get('emulated/idp', 'StudentSystemServices\Shibboleth\Controllers\ShibbolethController@emulateIdp');
    Route::post('emulated/idp', 'StudentSystemServices\Shibboleth\Controllers\ShibbolethController@emulateIdp');
    Route::get('emulated/login', 'StudentSystemServices\Shibboleth\Controllers\ShibbolethController@emulateLogin');
    Route::get('emulated/logout', 'StudentSystemServices\Shibboleth\Controllers\ShibbolethController@emulateLogout');
});
