# Laravel Shibboleth Service Provider

This package provides Shibboleth authentication for Laravel.

For development, it can _emulate_ an IdP (via [mrclay/shibalike][2]).

## Installation

Use [composer][1] to require the latest release into your project.
First add the package repository to your composer.json
```json
"repositories": [
    {
        "type": "vcs",
        "url": "https://gitlab.unige.ch/eLearning/gwybodaeth/laravel-shibboleth.git"
    }
]
```

Then require the latest release in your project:
    composer require unige/laravel-shibboleth

If you you would like to use the emulated IdP via shibalike, then you will need
to manually register it.

```php
StudentSystemServices\Shibboleth\ShibalikeServiceProvider::class,
```

_Note_ that the password is the same as the username for shibalike.

Publish the default configuration file:

    php artisan vendor:publish --provider="StudentSystemServices\Shibboleth\ShibbolethServiceProvider"

Optionally, you can also publish the views for the shibalike emulated IdP login:

    php artisan vendor:publish --provider="StudentSystemServices\Shibboleth\ShibalikeServiceProvider"

Change the driver to `shibboleth` in your `config/auth.php` file.

```php
'providers' => [
    'users' => [
        'driver' => 'shibboleth',
        'model'  => App\User::class,
    ],
],
```
On the Shibboleth side you need to protect the route `/shibboleth-authenticate`

Now users may login via Shibboleth by going to
`https://example.com/shibboleth-login` and logout using
`https://example.com/shibboleth-logout` so you can provide a custom link or
redirect based on email address in the login form.

```php
@if (Auth::guest())
    <a href="/shibboleth-login">Login</a>
@else
    <a href="/shibboleth-logout">
        Logout {{ Auth::user()->name }}
    </a>
@endif
```

You may configure server variable mappings in `config/shibboleth.php` such as
the user's first name, last name, entitlements, etc. You can take a look at them
by reading what's been populated into the `$_SERVER` variable after
authentication.

```php
<?php print_r($_SERVER);
```

Mapped values will be synced to the user table upon successful authentication.

### Declare Login Route

By convention, [laravel assumes a route named `login` exists][laravel-login]
to redirect unauthenticated requests.

[laravel-login]:https://github.com/laravel/framework/blob/10.x/src/Illuminate/Foundation/Exceptions/Handler.php#L570

This package names its route `shibboleth-login` because
it's designed to work alongside other authentication providers,
such as the default scaffolding provided by artisan.
But if this is the only authentication provider,
then that name will need to be manually declared. e.g.

```php
Route::name('login')->get('/login', '\\'.Route::getRoutes()->getByName('shibboleth-login')->getActionName());
```

or more readable, but with a redirect:

```php
Route::redirect('/login', '/shibboleth-login')->name('login');
```

## Authorization

You can check for an entitlement string of the current user statically:

```php
$entitlement = 'urn:mace:uark.edu:ADGroups:Computing Services:Something';

if (Entitlement::has($entitlement)) {
    // authorize something
}
```

Now you can draft [policies and gates][3] around these entitlements.

## Local Users

This was designed to work side-by-side with the native authentication system for
projects where you want to have both Shibboleth and local users. If you would
like to allow local registration as well as authenticate Shibboleth users, then
use laravel's built-in auth system.

    php artisan make:auth


[1]: https://getcomposer.org/
[2]: https://github.com/mrclay/shibalike
[3]: https://laravel.com/docs/10.x/authorization
